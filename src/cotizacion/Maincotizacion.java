/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion;


 
//ulises//

public class Maincotizacion {

    public static void main(String[] args) {
        // crear objeto de cotizacion por omisión
        cotizacion cotizacionPorOmicion = new cotizacion();
        
        cotizacionPorOmicion.setNucotizacion("123");
        cotizacionPorOmicion.setDescripAuto("Auto");
        cotizacionPorOmicion.setPrecio(220000);
        cotizacionPorOmicion.setPorcentajeImpuesto(25);
        cotizacionPorOmicion.setPlazo(36);
        
        

        // mostrar información del objeto
        System.out.println("Cotización por Omisión");
        System.out.println("Numero de cotizacion" + cotizacionPorOmicion.getNucotizacion());
        System.out.println("Descripción del Auto: " + cotizacionPorOmicion.getDescripAuto());
        System.out.println("Precio: " + cotizacionPorOmicion.getPrecio());
        System.out.println("Porcentaje de Impuesto: %" + cotizacionPorOmicion.getPorcentajeImpuesto());
        System.out.println("Plazo: " + cotizacionPorOmicion.getPlazo());

        // Calcular y mostrar el pago de impuesto, financiamiento y mensualidad 
        System.out.println("Pago inicial: " + cotizacionPorOmicion.calcularPagoImpuesto(0, 0));
        System.out.println("Financiamiento: " + cotizacionPorOmicion.calcularFinanciamiento(0, 0));
        System.out.println("Mensualidad: " + cotizacionPorOmicion.calcularMensualidad(0, 0, 0));
    }
}